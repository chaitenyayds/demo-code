import urllib3
import certifi
from urllib.parse import urlencode
import json
import pprint

http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED',ca_certs=certifi.where())
data = {"query":"blockchain","queryExpression":"","filters":[],"orderBy":0,"skip":0,"sortAscending":"true","take":100}
encoded_data = json.dumps(data).encode('utf-8')

r = http.request(
    'POST',
    'https://academic.microsoft.com/api/search',
    body=encoded_data,
    headers={'Content-Type': 'application/json'})

jsonData = json.loads(r.data.decode('utf-8'))

pprint.pprint(jsonData)
